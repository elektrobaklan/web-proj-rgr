<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\GamesController;
use App\Http\Controllers\ImagesController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ManagerController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SiteInfoController;
use Illuminate\Support\Facades\Route;

Route::get('/language/{locale}', function ($locale) {
    session(['locale' => $locale]);
    return redirect()->back();
})->name('language.switch');


Route::get('/comments/{id}', [CommentsController::class, 'get'])->name('comments.get');
Route::get('/img/{filename}', [ImagesController::class, 'getByName'])->name('image.getByName');
Route::get('/img/{id}', [ImagesController::class, 'getById'])->name('image.getById');
Route::get('/games/{id}', [GamesController::class, 'get'])->name('games.get');
Route::get('/registration', [RegistrationController::class, 'index'])->name('user.index');
Route::post('/register', [RegistrationController::class, 'register'])->name('user.register');
Route::get('/login', [LoginController::class, 'index'])->name('user.index');
Route::post('/login', [LoginController::class, 'login'])->name('user.login');
Route::get('/main', [MainController::class, 'index'])->name('main.index');
Route::get('/agreement', [SiteInfoController::class, 'index'])->name('site.agreement');

Route::group(['middleware' => ['auth']], function () {
    Route::post('/comments', [CommentsController::class, 'insert'])->name('comments.post');
    Route::put('/comments', [CommentsController::class, 'update'])->name('comments.update');
    Route::delete('/comments/{id}', [CommentsController::class, 'delete'])->name('comments.delete');
    Route::post('/img', [ImagesController::class, 'insert'])->name('image.post');
    Route::delete('/img/{id}', [ImagesController::class, 'delete'])->name('image.delete');
    Route::put('/games', [GamesController::class, 'update'])->name('games.update');
    Route::delete('/games/{id}', [GamesController::class, 'delete'])->name('games.delete');
    Route::get('/logout', [LoginController::class, 'logout'])->name('user.logout');
    Route::get('/search/{query}', [SearchController::class, 'index'])->name('search.index');
    Route::get('/event{id}', [EventsController::class, 'index'])->name('event.get');
    Route::get('/game', [SearchController::class, 'index'])->name('game.get');
    Route::get('/events/count', [MainController::class, 'total'])->name('events.size');
});

Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
    Route::post('/registration', [AdminController::class, 'register'])->name('admin.manager.create');
});

Route::group(['middleware' => ['manager']], function () {
    Route::get('/manager', [ManagerController::class, 'index'])->name('manager.index');
    Route::post('/manager/category', [ManagerController::class, 'category'])->name('categories.post');
    Route::get('/categories', [CategoriesController::class, 'get'])->name('categories.get');
    Route::get('/games', [GamesController::class, 'all'])->name('games.all');
    Route::post('/manager/game', [ManagerController::class, 'insert'])->name('games.post');
    Route::post('/manager/event', [ManagerController::class, 'create'])->name('events.post');
});