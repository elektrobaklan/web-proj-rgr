import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/main.css', 'resources/css/login.css',
                'resources/css/manager.css', 'resources/css/admin.css',
                'resources/css/registration.css', 'resources/js/app.js',
                'resources/js/main.js', 'resources/js/manager.js',
                'resources/css/games.css', 'resources/css/event.css'
            ],
            refresh: true,
        }),
    ],
});
