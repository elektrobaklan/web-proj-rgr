@extends('template')

@section('head')
<title>Менеджер</title>
@vite(['resources/css/main.css', 'resources/css/manager.css', 'resources/js/main.js', 'resources/js/manager.js'])
@endsection

@section('main')
<div class="tabs">
    <div class="tab active" data-tab="tab1">Создание игры</div>
    <div class="tab" data-tab="tab2">Создание категории</div>
    <div class="tab" data-tab="tab3">Создание события</div>
</div>

<div class="tab-content active show" id="tab1">
    <form class="game-form" action="{{ route('games.post') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="title">Название игры</label>
            <input type="text" id="title" name="title" placeholder="Введите название игры" required>
            <div class="error-contact">@if ($errors->has('title')) {{ $errors->first('title') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="description">Описание игры</label>
            <textarea id="description" name="description" placeholder="Введите описание игры" required></textarea>
            <div class="error-contact">@if ($errors->has('text')) {{ $errors->first('description') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="category">Категория игры</label>
            <select id="category" name="category" required>
                <option value="">Выберите категорию</option>
            </select>
            <div class="error-contact">@if ($errors->has('category')) {{ $errors->first('category') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="tags">Теги игры</label>
            <input type="text" id="tags" name="tags" placeholder="Введите теги через запятую" required>
            <div class="error-contact">@if ($errors->has('tags')) {{ $errors->first('tags') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="images">Картинки игры</label>
            <input type="file" id="images" name="images[]" multiple accept="image/*" required>
            <div class="error-contact">@if ($errors->has('images[]')) {{ $errors->first('images[]') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="author">Автор игры</label>
            <input type="text" id="author" name="author" placeholder="Введите автора игры" required>
            <div class="error-contact">@if ($errors->has('author')) {{ $errors->first('author') }} @endif</div>
        </div>

        <button type="submit">Создать игру</button>
    </form>
</div>

<div class="tab-content" id="tab2">
    <form class="game-form" action="{{ route('categories.post') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="name">Название категории</label>
            <input type="text" id="name-category" name="name" placeholder="Введите название категории" required>
        </div>

        <button type="submit">Создать категорию</button>
    </form>
</div>

<div class="tab-content" id="tab3">
    <form class="game-form" action="{{ route('events.post') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="title">Заголовок события</label>
            <input type="text" id="title" name="title" placeholder="Введите заголовок события" required>
            <div class="error-contact">@if ($errors->has('title')) {{ $errors->first('title') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="text">Описание события</label>
            <textarea id="text" name="text" placeholder="Введите описание события" required></textarea>
            <div class="error-contact">@if ($errors->has('text')) {{ $errors->first('text') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="game">Игра</label>
            <select id="game" name="game" required>
                <option value="">Выберите игру</option>
            </select>
            <div class="error-contact">@if ($errors->has('game')) {{ $errors->first('game') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="video-url">Ссылка на видео</label>
            <input type="text" id="video-url" name="video_url" placeholder="Введите ссылку на видео" required>
            <div class="error-contact">@if ($errors->has('video-url')) {{ $errors->first('video-url') }} @endif</div>
        </div>

        <div class="form-group">
            <label for="image-url">Ссылка на картинку</label>
            <input type="text" id="image-url" name="image_url" placeholder="Введите ссылку на картинку" required>
            <div class="error-contact">@if ($errors->has('image-url')) {{ $errors->first('image-url') }} @endif</div>
        </div>

        <button type="submit">Создать событие</button>
    </form>
</div>

@endsection