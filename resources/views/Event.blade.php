@extends('template')

@section('head')
<title>Событие</title>
@vite(['resources/css/main.css', 'resources/js/main.js', 'resources/css/event.css'])
@endsection

@section('main')
<div class="news-item">
    <h2 class="news-item__title">{{ $data['title'] }}</h2>
    <div class="news-item__content">
        <div class="news-item__text">
            <div class="news-item__image">
                <img src="{{ $data['image_url'] }}" alt="">
            </div>
            <p>{{ $data['text'] }}</p>
            <div class="news-item__video">
                <iframe src="https://www.youtube.com/embed/{{ $data['video_url'] }}"></iframe>
            </div>
        </div>
    </div>
</div>
@endsection