@extends('template')

@section('head')
<title>Авторизация</title>
@vite(['resources/css/login.css', 'resources/css/main.css'])
@endsection

@section('main')
<div class="container-login">
    <h1>Авторизация пользователя</h1>
    <form action="{{ route('user.login') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="text" name="email" id="email" required>
            @error('email')
            <span class="error">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="password">Пароль:</label>
            <input type="password" name="password" id="password" required>
            @error('password')
            <span class="error">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <button type="submit">Войти</button>
        </div>
    </form>
    <div class="register-link">
        <a href="http://localhost:8000/registration">Нет аккаунта? Зарегистрируйтесь</a>
    </div>
</div>
@endsection