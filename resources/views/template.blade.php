<!DOCTYPE html>
<html>

<head>
    @yield('head')
    @vite(['resources/js/main.js'])
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <div class="container">
        <header>
            <div class="header-container">
                <div id="header-left">
                    <img src="{{ route('image.getByName', ['filename' => 'image.gif']) }}" alt="Game 1">
                    <div class="search-box">
                        <input type="text" class="search-input" placeholder="@lang('messages.search')" id="search">
                    </div>
                </div>
                <nav>
                    <h1 id="main-name">@if (!is_null(auth()->user())) {{ auth()->user()->name }} @endif</h1>
                    <ul>
                        <li class="{{ Request::route()->getName() == 'main.index' ? 'active' : '' }}"><a href="{{ route('main.index') }}">@lang('messages.main')</a></li>
                        @if (!is_null(auth()->user()))
                        @if (auth()->user()->isAdmin())
                        <li class="{{ Request::route()->getName() == 'admin.index' ? 'active' : '' }}"><a href="{{ route('admin.index') }}">@lang('messages.panel')</a></li>
                        @elseif (auth()->user()->isManager())
                        <li class="{{ Request::route()->getName() == 'manager.index' ? 'active' : '' }}"><a href="{{ route('manager.index') }}">@lang('messages.editor')</a></li>
                        @endif
                        @endif
                        <li class="{{ Request::route()->getName() == 'popular.index' ? 'active' : '' }}"><a href="#">@lang('messages.popular')</a></li>
                        <li class="{{ Request::route()->getName() == 'search.index' ? 'active' : '' }}"><a href="http://localhost:8000/search/all">@lang('messages.allgames')</a></li>
                        <li id="loginout-bullet">
                            @if (!is_null(auth()->user()))
                            <a href="http://localhost:8000/logout">@lang('messages.logout')</a>
                            @else
                            <a href="http://localhost:8000/login">@lang('messages.login')</a>
                            @endif
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

        <main>
            @yield('main')
        </main>

        <footer>
            <div class="footer-container">
                <p>Good Game &copy; @lang('messages.rights')</p>
                <div id="right-footer">
                    <ul>
                        <li><a href="{{ route('site.agreement') }}">@lang('messages.about')</a></li>
                        <li><a href="{{ route('site.agreement') }}">@lang('messages.rules')</a></li>
                        <li><a href="{{ route('site.agreement') }}">@lang('messages.politic')</a></li>
                    </ul>
                    <div class="country-select">
                        <select id="country" name="country">
                            <option value="">Выберите язык</option>
                            <option value="en" {{ session('locale') == 'en' ? 'selected' : '' }}>English</option>
                            <option value="ru" {{ session('locale') == 'ru' ? 'selected' : '' }}>Русский</option>
                        </select>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>