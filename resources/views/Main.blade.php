@extends('template')

@section('head')
<title>Каталог игр</title>
@vite(['resources/css/main.css', 'resources/js/main.js', 'resources/css/login.css', 'resources/css/paging.css', 'resources/js/paging.js'])
@endsection
@section('main')
<section class="content">
    <h2>Последние события</h2>
    @foreach ($data as $event)
    <div class="item">
        <a href="{{ route('event.get', ['id' => $event['id']]) }}">
            <h3>{{ $event['title'] }}</h3>
            <p>{{ $event['text'] }}</p>
        </a>
    </div>
    <hr class="separator">
    @endforeach
    <div class="item">
        <h3>Nullam et luctus massa</h3>
        <p>Maecenas nibh lacus, tincidunt quis dolor nec, fringilla dapibus lorem.
            Donec molestie eu velit at malesuada. Proin mattis est a mauris tempus consequat.
            Nullam et luctus massa. Pellentesque tincidunt orci quis posuere gravida. Mauris
            iaculis ipsum ut fermentum gravida. Curabitur non elit congue, laoreet purus sit amet,
            tincidunt est. Vestibulum aliquet tortor suscipit dolor venenatis varius. Sed vitae lorem vel
            libero elemenзtum ornare a ac augue. Curabitur et iaculis libero. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit. In hac habitasse platea dictumst. Integer in rutrum dolor. Duis libero mauris,
            suscipit sed interdum et, blandit et sapien.</p>
    </div>
    <hr class="separator">
    <div class="item">
        <a href="полный_ссылка_на_видео">
            <div class="video-preview" style="background-image: url('{{ route('image.getByName', ['filename' => 'image.gif']) }}')"></div>
            <h3>Заголовок видео 1</h3>
        </a>
    </div>
    <hr class="separator">
    <div class="item">
        <h3>Заголовок статьи 2</h3>
        <p>Текст статьи 2...</p>
    </div>
    <hr class="separator">
    <div class="item">
        <a href="полный_ссылка_на_видео">
            <div class="video-preview" style="background-image: url('{{ route('image.getByName', ['filename' => 'image.gif']) }}')"></div>
            <h3>Заголовок видео 2</h3>
        </a>
    </div>
</section>
<div class="pagination">
    <h3 class="paging-nav" id="prev">Предыдущая</h3>
    <ul id="paging-container"></ul>
    <h3 class="paging-nav" id="next">Следующая</h3>
</div>
@endsection