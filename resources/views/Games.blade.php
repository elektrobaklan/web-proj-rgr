@extends('template')

@section('head')
<title>Игра</title>
@vite(['resources/css/main.css', 'resources/js/main.js', 'resources/css/games.css'])
@endsection

@section('main')
<div class="game-list">
    @foreach ($data as $game)
    <div class="game">
        <h3 class="game-title">{{ $game['title'] }}</h3>
        <div class="game-description">{{ $game['description'] }}</div>
        <div class="game-category">{{ $game->category->name }}</div>
        <div class="game-images">
            @foreach ($game->images as $image)
            <img src="{{ $image->url }}" alt="">
            @endforeach
        </div>
        <div class="game-tags">
            @foreach ($game['tags'] as $tag)
            <span class="tag">{{ $tag }}</span>
            @endforeach
        </div>
    </div>
    <hr class="divider">
    @endforeach
</div>
@endsection