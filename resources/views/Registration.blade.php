@extends('template')

@section('head')
<title>Регистрация</title>
@vite(['resources/css/registration.css', 'resources/css/main.css'])
@endsection

@section('main')
<div class="container-register">
    <h1>Регистрация пользователя</h1>
    <form action="{{ route('user.register') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">ФИО:</label>
            <input type="text" name="name" id="name" required>
            @error('name')
            <span class="error">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" name="email" id="email" required>
            @error('email')
            <span class="error">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="password">Пароль:</label>
            <input type="password" name="password" id="password" required>
            @error('password')
            <span class="error">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <button type="submit" id="submit">Зарегистрироваться</button>
        </div>
    </form>
    <div class="login-link">
        <a href="http://localhost:8000/login">Уже зарегистрированы? Авторизуйтесь</a>
    </div>
</div>
@endsection