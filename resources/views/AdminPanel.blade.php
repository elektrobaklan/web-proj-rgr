@extends('template')

@section('head')
<title>Админ-панель</title>
@vite(['resources/css/main.css', 'resources/js/main.js', 'resources/css/admin.css'])
@endsection

@section('main')
<div class="admin-panel">
    <div class="user-list">
        <h2>Список пользователей-менеджеров</h2>
        <ul>
            <li>Пользователь 1</li>
            <li>Пользователь 2</li>
            <li>Пользователь 3</li>
        </ul>
    </div>
    <div class="add-user">
        <h2>Добавить пользователя-менеджера</h2>
        <form action="{{ route('admin.manager.create') }}" method="post">
            @csrf
            <input type="text" placeholder="Имя" name="name">
            <input type="text" placeholder="Email пользователя" name="email">
            <input type="password" placeholder="Пароль" name="password">
            <button type="submit">Добавить</button>
        </form>
    </div>
</div>
@endsection