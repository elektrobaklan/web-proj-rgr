<?php
return [
    'politic' => 'Privacy Policy',
    'about' => 'About Us',
    'rules' => 'Terms of Use',
    'rights' => '2023 All rights reserved',
    'search' => 'Games search',
    'main' => 'Main',
    'editor' => 'Editor',
    'popular' => 'Popular',
    'allgames' => 'All games',
    'panel' => 'Panel',
    'logout' => 'Log out',
    'login' => 'Log in'
];
