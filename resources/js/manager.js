const selectElement = $('#category');
const selectGameElement = $('#game');

loadCategories();
loadGames();

$(document).ready(function () {
    $('.tab').click(function () {
        const tabId = $(this).data('tab');
        const tabContent = $('#' + tabId);

        $('.tab').removeClass('active');
        $('.tab-content').removeClass('show');

        $(this).addClass('active');
        tabContent.addClass('show');
    });


});

function loadCategories() {
    $.ajax({
        type: "GET",
        url: "http://localhost:8000/categories",
        success: function (response) {
            const categories = response.data
            categories.forEach(function (category) {
                const option = $('<option></option>')
                    .val(category.id)
                    .text(category.name);

                selectElement.append(option);
            });
        }
    });
}

function loadGames() {
    $.ajax({
        type: "GET",
        url: "http://localhost:8000/games",
        success: function (response) {
            const games = response.data
            games.forEach(function (game) {
                const option = $('<option></option>')
                    .val(game.id)
                    .text(game.title);

                selectGameElement.append(option);
            });
        }
    });
}
