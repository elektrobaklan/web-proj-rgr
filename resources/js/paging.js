const pagingContainer = $('#paging-container');
const blogsContainer = $('#blogs-container');
const prevEl = $('#prev');
const nextEl = $('#next');

let fromIndex = 1;
let toIndex = 5;
let currentPosition = 1;
let eventsCount;

$(document).ready(function () {
    getEventsCount();
    placePages(fromIndex, toIndex, currentPosition);
});

prevEl.click(function (e) {
    e.preventDefault();
    getEventsCount()
    if (currentPosition > 1) {
        currentPosition--;
        if (currentPosition >= fromIndex && currentPosition <= toIndex) {
            colorSelected(currentPosition);
        } else {
            fromIndex = fromIndex - 5;
            toIndex = toIndex - 5;
            placePages(fromIndex, toIndex, currentPosition);
        }
        blogsContainer.html("");
        //loadPageData(currentPosition);
    }
});
nextEl.click(function (e) {
    e.preventDefault();
    getEventsCount();
    if (currentPosition < eventsCount) {
        currentPosition++;
        if (currentPosition >= fromIndex && currentPosition <= toIndex) {
            colorSelected(currentPosition);
        } else {
            if (toIndex == eventsCount - 1) {
                fromIndex = fromIndex + 5;
                toIndex = toIndex + 5;
                placePages(eventsCount, eventsCount, currentPosition);
                return;
            }
            fromIndex = fromIndex + 5;
            toIndex = toIndex + 5;
            placePages(fromIndex, toIndex, currentPosition);
        }
        blogsContainer.html("");
        //loadPageData(currentPosition);
    }
});


function placePages(fromIndex, toIndex, currentPosition) {
    pagingContainer.html("");
    for (let index = fromIndex; index <= toIndex; index++) {
        const liElem = $('<li>');
        const aElem = $('<a>');
        aElem.text(index);
        aElem.data('page', index);
        liElem.append(aElem);
        pagingContainer.append(liElem);
    }
    colorSelected(currentPosition);
}

function colorSelected(currentPosition) {
    pagingContainer.children().each(function (index, element) {
        $(this).children().eq(0).css('background-color', '#f5f5f5');
    });
    const currItem = pagingContainer.find(':contains("' + currentPosition + '")');
    currItem.css('background-color', '#85d8');
}

pagingContainer.on('click', 'a', function (e) {
    e.preventDefault();
    const page = $(this).data('page');
    currentPosition = page;
    colorSelected(currentPosition);
    blogsContainer.html("");
    //loadPageData(page);
});

function loadPageData(page) {
    $.ajax({
        url: 'http://localhost:8000/myblog',
        data: { page: page },
        success: function (data) {
            if (data.length == 0) {
                const dateElem = $('<div>');
                dateElem.text("Пусто");
                dateElem.addClass('blog-post-date');
                blogsContainer.append(dateElem);
                return;
            }
            data.forEach(element => {
                const blogContainer = $('<div>');
                blogContainer.addClass('blog-post');
                blogContainer.attr('data-id', element['id']);

                const dateElem = $('<div>');
                dateElem.text(element['created_at']);
                dateElem.addClass('blog-post-date');

                const titleElem = $('<div>');
                titleElem.text(element['title']);
                titleElem.addClass('blog-post-title');
                titleElem.attr('data-id', element['id']);

                const editElem = $('<div>');
                editElem.text('Изменить');
                editElem.data('id', element['id']);
                editElem.addClass('blog-post-edit');

                const deleteElem = $('<div>');
                deleteElem.text('Удалить');
                deleteElem.data('id', element['id']);
                deleteElem.addClass('blog-post-delete');

                const comments = element['comments'];
                const commentsElems = [];
                if (comments.length > 0) {
                    comments.forEach(comment => {
                        const commentElem = $('<div>');
                        commentElem.addClass('comment');
                        commentElem.append($('<div>').addClass('comment-author').text(comment['author']));
                        commentElem.append($('<div>').addClass('comment-text').text(comment['text']));
                        commentElem.append($('<div>').addClass('comment-date').text(comment['created_at']));
                        commentsElems.push(commentElem);
                    });
                }

                const commentButton = $('<button>');
                commentButton.addClass('blog-post-comment');
                commentButton.text('Комментировать');
                commentButton.attr('data-id', element['id']);
                const id = element['id'];
                commentButton.click(function (e) {
                    const targetEl = e.target;
                    if (targetEl.closest("button.blog-post-comment")) {
                        console.log(targetEl);
                        openCreateComment(targetEl, id);
                    }
                });

                const titleAndEditContainer = $('<div>');
                titleAndEditContainer.css('flex-direction', 'row');
                titleAndEditContainer.css('display', 'flex');
                titleAndEditContainer.append(titleElem);
                titleAndEditContainer.append(editElem);
                titleAndEditContainer.append(deleteElem);

                const imageContainerElem = $('<div>');
                imageContainerElem.addClass('blog-post-image');

                const image = element['image'];
                if (image.includes('.png') || image.includes('.jpg')) {
                    const imageElem = $('<img>');
                    imageElem.attr('src', image);
                    imageContainerElem.append(imageElem);
                }

                const textElem = $('<div>');
                textElem.text(element['text']);
                textElem.addClass('blog-post-content');
                textElem.attr('data-id', element['id']);

                editElem.click(function (e) {
                    const targetElement = e.target;
                    if (targetElement.closest("div.blog-post-edit")) {
                        openEdit(targetElement, $(this).data('id'));
                    }
                });

                deleteElem.click(function (e) {
                    const targetElement = e.target;
                    if (targetElement.closest("div.blog-post-delete")) {
                        deleteBlog(targetElement, $(this).data('id'));
                    }
                });

                blogContainer.append(dateElem);
                blogContainer.append(titleAndEditContainer);
                blogContainer.append(imageContainerElem);
                blogContainer.append(textElem);
                blogContainer.append(commentsElems)
                blogContainer.append(commentButton);
                blogsContainer.append(blogContainer);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('Ошибка при загрузке данных: ' + textStatus + ' ' + errorThrown);
        }
    });
}

function getEventsCount() {
    $.ajax({
        url: '/events/count',
        type: 'GET',
        success: function (response) {
            eventsCount = response.count;
        },
        error: function (error) {
            console.log(error);
        }
    });
}