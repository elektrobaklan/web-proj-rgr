$(document).ready(function () {
    $('.item').hover(
        function () {
            $(this).css('background-color', '#dbd8d8');
        },
        function () {
            $(this).css('background-color', '#f1f1f1');
        }
    );
    $('#country').change(function () {
        const selectedLanguage = $(this).val();
        window.location.href = '/language/' + selectedLanguage;
    });
    $('#search').on('keydown', function (event) {
        if (event.keyCode === 13) {
            const searchQuery = $(this).val();
            let query;
            if (!searchQuery) query = 'all'
            else query = searchQuery
            window.location.href = 'http://localhost:8000/search/' + query
         }
    });
});