<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegistrationController extends Controller
{
    public function index()
    {
        return view('Registration');
    }

    public function register(RegistrationRequest $request)
    {
        if (User::where('email', $request->email)->exists()) {
            return redirect()->back();
        }

        event(new Registered($user = $this->create($request->all())));

        Auth::login($user);

        return redirect()->intended('/main');
    }

    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'name' => $data['name'],
            'role' => 'user',
            'password' => Hash::make($data['password']),
        ]);
    }
}
