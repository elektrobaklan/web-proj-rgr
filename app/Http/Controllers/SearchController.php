<?php

namespace App\Http\Controllers;

use App\Models\Games;
use Illuminate\Support\Facades\Storage;

class SearchController extends Controller
{
    public function index($query)
    {
        if ($query == 'all') {
            $games = Games::with(['images', 'category'])->get();
        } else {
            $games = Games::with(['images', 'category'])
                ->where('title', 'like', "%{$query}%")
                ->get();
        }

        foreach ($games as $game) {
            foreach ($game->images as $image) {
                $image->url = Storage::url($image->url);
            }
        }
        return view('Games', ['data' => $games]);
    }
}
