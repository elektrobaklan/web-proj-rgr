<?php

namespace App\Http\Controllers;

use App\Models\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MainController extends Controller
{
    public function index(Request $request)
    {
        $events = Events::latest()->take(5)->get();
        $data = [];
        foreach ($events as $event) {
            $data[] = [
                "id" => $event->id,
                "title" => $event->title,
                "image" => str_replace("public", "", Storage::disk('public')->url($event->image)),
                "text" => $event->text,
                "tags" => (array) $event->tags,
                "created_at" => $event->created_at->format('m/d/Y'),
            ];
        }
        $user = auth()->user();

        if ($request->ajax()) {
            return response()->json($data);
        } else {
            return view('Main', ['name' => $user->name ?? null, 'data' => $data]);
        }
    }

    public function total()
    {
        $count = Events::count();
        response()->json([
            "items_count" => $count
        ]);
    }
}
