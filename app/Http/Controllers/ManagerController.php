<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventsRequest;
use App\Http\Requests\GamesRequest;
use App\Models\Categories;
use App\Models\Events;
use App\Models\Games;
use Illuminate\Http\Request;

class ManagerController extends Controller
{
    public function index()
    {
        return view('ManagerPanel');
    }

    public function category(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:3|max:100'
        ]);

        Categories::create($validatedData);
        return redirect()->back()->with('sucess', 'Category was created');
    }

    public function insert(GamesRequest $request)
    {
        $validatedData = $request->validated();

        $game = Games::create([
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'tags' => $validatedData['tags'],
            'author' => $validatedData['author'],
            'category_id' => $validatedData['category']
        ]);

        if ($request->hasFile('images')) {
            $images = [];
            foreach ($request->file('images') as $image) {
                $path = $image->store('public/images');
                $url = str_replace('public/', '', $path);
                $images[] = ['url' => $url];
            }
            $game->images()->createMany($images);
        }

        return redirect()->back()->with('success', 'Игра успешно создана.');
    }

    public function create(EventsRequest $request)
    {
        $validatedData = $request->validated();

        Events::create([
            'title' => $validatedData['title'],
            'text' => $validatedData['text'],
            'image_url' => $validatedData['image_url'],
            'video_url' => $validatedData['video_url'],
            'user_id' => auth()->user()->id(),
            'game_id' => $validatedData['game']
        ]);

        return redirect()->back()->with('success', 'Событие успешно создано.');
    }
}
