<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentsRequest;

class CommentsController extends Controller
{
    public function get($id)
    {
        # code...
    }

    public function insert(CommentsRequest $request)
    {
        # code...
    }

    public function update(CommentsRequest $request, $id)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }
}
