<?php

namespace App\Http\Controllers;

use App\Models\Categories;

class CategoriesController extends Controller
{
    public function get()
    {
        $categories = Categories::all();
        return response()->json([
            'data' => $categories
        ]);
    }
}
