<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index()
    {
        return view('AdminPanel');
    }

    public function register(RegistrationRequest $request)
    {
        if (User::where('email', $request->email)->exists()) {
            return redirect()->back();
        }

        event(new Registered($user = $this->create($request->all())));

        return redirect('/');
    }

    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'name' => $data['name'],
            'role' => 'manager',
            'password' => Hash::make($data['password']),
        ]);
    }
}
