<?php

namespace App\Http\Controllers;

use App\Models\Events;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    public function index($id)
    {
        $event = Events::findOrFail($id);
        preg_match('/v=([a-zA-Z0-9_-]+)/', $event->video_url, $matches);
        $id = $matches[1];
        $data = [
            'title' => $event->title,
            'text' => $event->text,
            'image_url' => $event->image_url,
            'video_url' => $id,
            'game_id' => $event->game_id,
            'author' => $event->user_id,
        ];
        return view('Event', ['data' => $data]);
    }
}
