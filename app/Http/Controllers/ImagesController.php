<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImagesController extends Controller
{
    public function getByName($filename)
    {
        $path = public_path('images/' . $filename);

        if (file_exists($path)) {
            return response()->file($path);
        }

        return response()->file(public_path('images/.gif'));
    }


    public function getById($id)
    {
        # code...
    }

    public function insert(Request $request)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }
}
