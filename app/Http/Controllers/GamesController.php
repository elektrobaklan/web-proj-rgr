<?php

namespace App\Http\Controllers;

use App\Http\Requests\GamesRequest;
use App\Models\Games;

class GamesController extends Controller
{
    public function index()
    {
        return view('Game');
    }

    public function get($id)
    {
        # code...
    }

    public function all()
    {
        $games = Games::all();
        return response()->json([
            'data' => $games
        ]);
    }

    public function update(GamesRequest $request, $id)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }
}
