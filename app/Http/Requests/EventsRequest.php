<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EventsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required',
            'text' => 'required',
            'image_url' => 'required',
            'video_url' => 'required',
            'game' => 'required'
        ];
    }
}
