<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'text' => 'required|string|min:3|max:1000',
            'author' => 'required|string|max:1000|min:3',
            'event_id' => 'required',
        ];
    }
}
