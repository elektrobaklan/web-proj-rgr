<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|min:3|max:50',
            'email' => 'required|min:3|max:50|email',
            'password' => 'required|min:3|max:50'
        ];
    }
}
