<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'tags', 'author', 'category_id'];

    protected $table = 'games';

    protected $casts = ['tags' => 'json'];

    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = json_encode(explode(',', $value));
    }

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function images()
    {
        return $this->hasMany(Images::class, 'game_id');
    }

    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id', 'id');
    }

    public function events()
    {
        return $this->hasMany(Events::class);
    }
}
