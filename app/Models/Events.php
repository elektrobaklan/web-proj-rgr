<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'text', 'game_id', 'video_url', 'image_url', 'user_id'];

    protected $table = 'events';

    public function game()
    {
        return $this->belongsTo(Games::class, 'game_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
