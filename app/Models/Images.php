<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    use HasFactory;

    protected $fillable = ['url', 'game_id'];

    protected $table = 'images';

    public function game()
    {
        return $this->belongsTo(Games::class, 'game_id');
    }
}
